var x=150;
var txtx=150;
var txty=75;
var y=75;
var angle=0;
var color='black';
var pen=1;
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
ctx.lineWidth=0.55;
function text1(){
	this.draw();
}
function text()
{
	this.erase();
	var x=document.getElementById("con2");
	var a=x.value.split(" ");
	for(var i=0;i<a.length;i++)
	{
		if(a[i]=="fd" || a[i]=="forward")
	{
		if(!isNaN(a[i+1]))
			this.forward(a[i+1]);
		else
			alert("Invalid Command");
	}
	else if(a[i]=="bk" || a[i]=="back")
	{
		if(!isNaN(a[i+1]))
			this.backward(a[i+1]);
		else
			alert("Invalid Command");
	}
	else if(a[i]=="rt" || a[i]=="right")
	{
		if(!isNaN(a[i+1]))
			this.right(a[i+1]);
		else
			alert("Invalid Command");
	}
	else if(a[i]=="lt" || a[i]=="left")
	{
		if(!isNaN(a[i+1]))
			this.left(a[i+1]);
		else
			alert("Invalid Command");
	}
	else if(a[i]=="setpc")
		this.color=a[i+1];
	else if(a[i]=="pu")
		this.pen=0;
	else if(a[i]=="pd")
		this.pen=1;
	else if(a[i]=="turtletext" || a[i]=="tt")
	{	
		var txt;
		for(var i=1;i<a.length;i++)
		{
			if(i>1)
			{
				txt=txt+" "+a[i];
			}
			else
				txt=a[i];
		}
		var txt1=txt.slice(1,-1);
		this.tt(txt1);
	}
	else if(a[i]=="setp")
	{
		this.right(a[i+2]);
		this.forward(a[i+1]);
	}
	else if(a[i]=="home")
	{
		this.x=150;
		this.y=75;
		this.txtx=this.x;
		this.txty=this.y;
	}
	else if(a[i]=="clear")
		this.clear();
/*	else
		alert("The command is invalid");*/
	}
	this.draw();
	document.getElementById("form").reset();
}
function draw(){
	ctx.beginPath();
	ctx.strokeStyle="blue";
	ctx.moveTo(this.txtx,this.txty);
	ctx.lineTo(this.txtx+5*Math.cos(this.angle*3.14/180),this.txty+5*Math.cos(this.angle*3.14/180));
	ctx.moveTo(this.txtx,this.txty);
	ctx.lineTo(this.txtx-5*Math.cos(this.angle*3,14/180),this.txty+5*Math.cos(this.angle*3.14/180));
	ctx.moveTo(this.txtx-5*Math.cos(this.angle*3.14/180),this.txty+5*Math.cos(this.angle*3.14/180));
	ctx.lineTo(this.txtx+5*Math.cos(this.angle*3.14/180),this.txty+5*Math.cos(this.angle*3.14/180));
	ctx.stroke();
}
function clear(){
	location.reload();
}
function erase()
{
	ctx.beginPath();
	ctx.strokeStyle="white";
	ctx.moveTo(this.txtx,this.txty);
	ctx.lineTo(this.txtx+5,this.txty+5);
	ctx.moveTo(this.txtx,this.txty);
	ctx.lineTo(this.txtx-5,this.txty+5);
	ctx.moveTo(this.txtx-5,this.txty+5);
	ctx.lineTo(this.txtx+5,this.txty+5);
	ctx.stroke();
}
function forward(t)
{
ctx.beginPath();
ctx.moveTo(this.x,this.y);
this.x=this.x+Number(t)*Math.sin(angle*3.14/180);
this.y=this.y-Number(t)*Math.cos(angle*3.14/180);
this.txtx=this.txtx+Number(t)*Math.sin(angle*3.14/180);
this.txty=this.txty-Number(t)*Math.cos(angle*3.14/180);
if(this.pen==1)
{
ctx.lineTo(this.x,this.y);
}
ctx.strokeStyle=color;
ctx.stroke();
}
function backward(t)
{
ctx.beginPath();
ctx.moveTo(this.x,this.y);
this.x=this.x-Number(t)*Math.sin(angle*3.14/180);
this.y=this.y+Number(t)*Math.cos(angle*3.14/180);
this.txtx=this.txtx-Number(t)*Math.sin(angle*3.14/180);
this.txty=this.txty+Number(t)*Math.cos(angle*3.14/180);
if(this.pen==1)
{
ctx.lineTo(this.x,this.y);
}
ctx.strokeStyle=color;
ctx.stroke();
}
function right(t)
{
this.angle=this.angle+Number(t);
}
function left(t)
{
this.angle=this.angle-Number(t);
}
function tt(txt)
{
	ctx.fillText(txt,this.x,this.y);
}
