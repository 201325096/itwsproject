var x;
var y;
var angle=0;
var color='black';
var pen=1;
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
var p=document.getElementById("tur");
var ptx=p.getContext("2d");
ctx.lineWidth=2;
ctx.font="30px Arial";
var off=c.getBoundingClientRect();
function main()
{
	c=document.getElementById("con1");
	ctx=c.getContext("2d");
	p=document.getElementById("tur");
	ptx=p.getContext("2d");
	/* setting position of turtle canvas */
	this.x=(c.width)/2;
	this.y=(c.height)/2;
	p.style.top = off.top+(c.height-p.height)/2+'px';
	p.style.left = off.left+(c.width-p.width)/2+'px';
	ptx.moveTo(12,0);
	ptx.strokeStyle="blue";
	ptx.lineWidth="2";
	ptx.lineTo(0,30);
	ptx.lineTo(24,30);
	ptx.lineTo(12,0)
	ptx.stroke();
}
function draw(t)
{
var num1=p.style.top.split("p");
var num2=p.style.left.split("p");
p.style.top=(Number(num1[0])-Math.cos(this.angle*3.14/180)*(t))+'px';
p.style.left=(Number(num2[0])+Math.sin(this.angle*3.14/180)*(t))+'px';
}
function text()
{
	var x=document.getElementById("con2");
	var a=x.value.split(" ");
	for(var i=0;i<a.length;i++)
	{
		if(a[i]=="REPEAT")
		{	a[2]=a[2].split("[")[1];
			a[a.length-1]=a[a.length-1].split("]")[0];
			for(var j=0;j<a[1];j++)
			{
				for(k=2;k<=a.length-1;k++)
				{
				if(a[k]=="fd" || a[k]=="forward")
				{
					if(!isNaN(a[k+1]))
						this.forward(a[k+1]);
					else
						alert("Invalid Command");
				}
				else if(a[k]=="bk" || a[k]=="back")
				{
					if(!isNaN(a[k+1]))
						this.backward(a[k+1]);
				else
					alert("Invalid Command");
				}
				else if(a[k]=="rt" || a[k]=="right")
				{
					if(!isNaN(a[k+1]))
						this.right(a[k+1]);
					else
						alert("Invalid Command");
				}
				else if(a[k]=="lt" || a[k]=="left")
				{
					if(!isNaN(a[k+1]))
						this.left(a[k+1]);
					else
						alert("Invalid Command");
				}
				else if(a[k]=="setpc")
					this.color=a[k+1];
				else if(a[k]=="pu" || a[k]=="penup")
					this.pen=0;
				else if(a[k]=="pd" || a[k]=="pendown")
					this.pen=1;
				else if(a[k]=="turtletext" || a[k]=="tt")
				{	
					var txt;
					for(var w=1;w<a.length;w++)
					{
						if(w>1)
						{
							txt=txt+" "+a[w];
						}
						else
							txt=a[w];
					}
					var txt1=txt.slice(1,-1);
					this.tt(txt1);
				}	
				else if(a[k]=="setp")
				{
					this.right(a[k+2]);
					this.forward(a[k+1]);
				}	
				else if(a[k]=="home")
				{
					this.x=150;
					this.y=75;
					this.angle=0;
					p.style.transform="rotate("+angle+"deg)";
					main();
				}
				else if(a[k]=="clear")
					this.clear();
			}
			
		}
		}
		else if(a[i]=="fd" || a[i]=="forward")
		{
			if(!isNaN(a[i+1]))
				this.forward(a[i+1]);
			else
				alert("Invalid Command");
		}
		else if(a[i]=="bk" || a[i]=="back")
		{
			if(!isNaN(a[i+1]))
				this.backward(a[i+1]);
			else
				alert("Invalid Command");
		}
		else if(a[i]=="rt" || a[i]=="right")
		{
			if(!isNaN(a[i+1]))
				this.right(a[i+1]);
			else
				alert("Invalid Command");
		}
		else if(a[i]=="lt" || a[i]=="left")
		{
			if(!isNaN(a[i+1]))
				this.left(a[i+1]);
			else
				alert("Invalid Command");
		}
		else if(a[i]=="setpc")
			this.color=a[i+1];
		else if(a[i]=="pu" || a[i]=="penup")
			this.pen=0;
		else if(a[i]=="pd" || a[i]=="pendown")
			this.pen=1;
		else if(a[i]=="turtletext" || a[i]=="tt")
		{	
			var txt;
			for(var i=1;i<a.length;i++)
			{
				if(i>1)
				{
					txt=txt+" "+a[i];
				}
				else
					txt=a[i];
			}
			var txt1=txt.slice(1,-1);
			this.tt(txt1);
		}
		else if(a[i]=="setp")
		{
			this.right(a[i+2]);
			this.forward(a[i+1]);
		}
		else if(a[i]=="home")
		{
			this.x=150;
			this.y=75;
			this.angle=0;
			p.style.transform="rotate("+angle+"deg)";
			main();
		}
		else if(a[i]=="clear")
			this.clear();
	}
	document.getElementById("form").reset();
}
function clear(){
	location.reload();
}
	
function forward(t)
{
ctx.beginPath();
ctx.moveTo(this.x,this.y);
this.x=this.x+Number(t)*Math.sin(angle*3.14/180);
this.y=this.y-Number(t)*Math.cos(angle*3.14/180);
if(this.pen==1)
{
ctx.lineTo(this.x,this.y);
}
ctx.strokeStyle=color;
ctx.stroke();
this.draw(t);
}
function backward(t)
{
ctx.beginPath();
ctx.moveTo(this.x,this.y);
this.x=this.x-Number(t)*Math.sin(angle*3.14/180);
this.y=this.y+Number(t)*Math.cos(angle*3.14/180);
if(this.pen==1)
{
ctx.lineTo(this.x,this.y);
}
ctx.strokeStyle=color;
ctx.stroke();
this.draw(-t);
}
function right(t)
{
this.angle=this.angle+Number(t);
rotateTurtle();
}
function left(t)
{
this.angle=this.angle-Number(t);
rotateTurtle();
}
function tt(txt)
{
ctx.fillStyle=color;
ctx.fillText(txt,this.x,this.y);
}
function rotateTurtle()
{
	p.style.transform = "rotate("+angle+"deg)";
}
