var x=150;
var y=75;
var angle=0;
function text()
{
	var x=document.getElementById("con2");
	var a=x.value.split(" ");
	if(a[0]=="fd" || a[0]=="forward")
	{
		this.forward(a[1]);
	}
	else if(a[0]=="bk" || a[0]=="back")
	{
		this.backward(a[1]);
	}
	else if(a[0]=="rt" || a[0]=="right")
	{
		this.right(a[1]);
	}
	else if(a[0]=="lt" || a[0]=="left")
	{
		this.left(a[1]);
	}
	document.getElementById("form").reset();
}
function forward(t)
{
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
ctx.moveTo(this.x,this.y);
this.x=this.x+Number(t)*Math.sin(angle*3.14/180);
this.y=this.y-Number(t)*Math.cos(angle*3.14/180);
ctx.lineTo(this.x,this.y);
ctx.stroke();
}
function backward(t)
{
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
ctx.moveTo(this.x,this.y);
this.x=this.x+Number(t)*Math.sin(angle*3.14/180);
this.y=this.y+Number(t)*Math.cos(angle*3.14/180);
ctx.lineTo(this.x,this.y);
ctx.stroke();
}
function right(t)
{
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
this.angle=this.angle+Number(t);
}
function left(t)
{
var c=document.getElementById("con1");
var ctx=c.getContext("2d");
this.angle=this.angle-Number(t);
}

