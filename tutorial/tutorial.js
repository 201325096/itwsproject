var zz=0,col=0;
//document.getElementById("form").reset();
var x=150,v=0;
var y=100;
var a;
var temp;
var temp1;
var c;
var ctx;
var off;
var p;
var ptx;
var color='black';
function main()
{
	c=document.getElementById("con1");
	ctx=c.getContext("2d");
	off=c.getBoundingClientRect();
	ctx.lineWidth=2;
	ctx.font="30px Arial";
	p=document.getElementById("tur");
	ptx=p.getContext("2d");
	p.style.top = off.top+(c.height-p.height)/2+'px';
	p.style.left=off.left+(c.width-p.width)/2+'px';
	this.zz=0;
	p.style.transform = "rotate("+this.zz+"deg)";
	ptx.moveTo(12,0);
	ptx.strokeStyle="blue";
	ptx.lineWidth="2";
	ptx.lineTo(0,30);
	ptx.lineTo(24,30);
	ptx.lineTo(12,0)
	ptx.stroke();
}
function draw(t)
{
var num1=p.style.top.split("p");
var num2=p.style.left.split("p");
p.style.top=(Number(num1[0])-Math.cos(this.zz*3.14/180)*(t))+'px';
p.style.left=(Number(num2[0])+Math.sin(this.zz*3.14/180)*(t))+'px';
}
function forward()
{
	c.width=c.width;
	this.main();
	ctx.moveTo(c.width/2,c.height/2);
	ctx.lineTo(c.width/2,c.height/2-50);
	if (col==1)
	{ctx.strokeStyle="red";}
	else if(col==2)
	{ctx.strokeStyle="white";}
	else 
	{ctx.strokeStyle="black";}
	ctx.stroke();
	this.draw(50);
	this.a="Moves the turtle forward by a given distance."
	this.temp="For example: The turtle moves forward by 50 units";
    this.temp1="Command is 'fd 50' or 'forward 50'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function backward()
{
	c.width=c.width;
	this.main();
	ctx.moveTo(c.width/2,c.height/2);
	ctx.lineTo(c.width/2,c.height/2+50);
	ctx.stroke();
	this.draw(-50);
	this.a="Moves the turtle backward by a given distance."
	this.temp="For example: The turtle moves backward by 50 units"
    this.temp1="Command is 'bk 50' or 'backward 50'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function forw()
{
if(v==0)
{ctx.moveTo(c.width/2,c.height/2);
ctx.lineTo(c.width/2,c.height/2-50);}
else if(v==1)
{ctx.moveTo(c.width/2,c.height/2-50);
ctx.lineTo(c.width/2,c.height/2-100);
}
if (col==1)
{ctx.strokeStyle="red";}
else if(col==2)
{ctx.strokeStyle="white";}
else
{ctx.strokeStyle="black";}
ctx.stroke();
this.draw(50);
}
function right()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){p.style.transform = "rotate("+90+"deg)"},1000);
	this.zz=90;
	this.a="Turns the turtle by a given angle in the clockwise direction." 
	this.temp="For example: The turtle turns by an angle of 90 degrees";
    this.temp1="Command is 'rt 90' or 'right 90'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function left()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){p.style.transform = "rotate("+-90+"deg)"},1000);
	this.zz=-90;
	this.a="Turns the turtle by a given angle in the anti-clockwise direction."
	this.temp="For example: The turtle turns by an angle of 90 degrees";
    this.temp1="Command is 'lt 90' or 'left 90'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function setpc()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){col=1;this.forw();col=0;},2000);
	this.a="Sets the pen color to the given color";
    this.temp="For Example: Pen colour is purple";
	this.temp1="Command is 'setpc red'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function turtletext()
{
	ctx.lineWidth=2;
	ctx.font="50px Arial";
	c.width=c.width;
	this.main();
	setTimeout(function(){ctx.fillText("Hi",c.width/2,c.height/2);},2000);
	this.a="Prints the given text";
	this.temp="For example: Hi is displayed";
	this.temp1="Command is 'tt [Hi]' or 'turtletext [Hi]'";
    document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function ang()
{
ctx.moveTo(c.width/2,c.height/2);
ctx.lineTo(c.width/2+50*Math.sin(45*3.14/180),c.height/2-50*Math.cos(45*3.14/180));
ctx.stroke();
this.zz=45;
this.draw(50);
}
function home()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){this.forw();rotateTurtle(90);},1000);
	setTimeout(function(){this.main()},2000);
	this.a="Brings the turtle back to it's initial co-ordinates";
	this.temp="Command is 'home'";
	this.temp1="When we move the turtle forward by 50 and then give 'home' command, the turtle comes back to the initial co-ordinates";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function clean()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){this.forw();rotateTurtle(90);},1000);
	setTimeout(function(){c.width=c.width;this.main();},2000);
	this.a="Clears the current graphics";
	this.temp="Command is 'clear'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+"</i>"+"</b>"+"</center>";
}	
function setp()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){this.rotateTurtle(45);this.ang();},1000);
	this.a="Turns the turtle by the given angle in the clockwise direction and then moves forward by the given distance";
	this.temp="For example: The turtle turns by an angle of 45 towards right and then moves forward by 50 units";
	this.temp1="Command is 'setp 50 45'";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function penup()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){col=2;this.forw();col=0;},2000);
	this.a="Puts the turtle's pen up. When the turtle moves, it does not draw a line ";
	this.temp="Command is 'pu' or 'penup'";
	this.temp1="When forward 50 command is given after givin penup, the turtle moves forward without drawing a line";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function pendown()
{
	c.width=c.width;
	this.main();
	setTimeout(function(){col=2;this.forw();col=0;v=1;this.forw();v=0;},2000);
	this.a="Puts the turtle's pen down and causes the turtle to draw a line when it moves.";
	this.temp="Command is 'pd' or 'pendown'";
	this.temp1="When 'penup' is given the turtle moves without drawing lines.For the lines to appear again 'pendown' command should be given";
	document.getElementById('aap').innerHTML="<center>"+"<b>"+"<i>"+this.a+"<br>"+this.temp+"<br>"+this.temp1+"</i>"+"</b>"+"</center>";
}
function refresh()
{
	location.reload();
}
function rotateTurtle(angle)
{
	p.style.transform = "rotate("+angle+"deg)";
}

